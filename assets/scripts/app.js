import * as THREE from 'three'
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
import { RGBELoader } from 'three/addons/loaders/RGBELoader.js'
import { ARButton } from 'three/addons/webxr/ARButton.js';
import { LoadingBar } from './LoadingBar.js';


class App {
    constructor() {
        const container = document.createElement('div')
        document.body.appendChild(container)
        container.style.width = '100vw'
        container.style.height = '100vh'

        this.loadingBar = new LoadingBar()
        this.loadingBar.visible = false

        this.scene = new THREE.Scene();

        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.01, 1000)
        this.camera.position.set(0, 1.6, 0)

        const ambient = new THREE.AmbientLight(0xffffff, 0xbbbbff, 1);
        ambient.position.set(0.5, 1, 0.25);
        this.scene.add(ambient);

        this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
        this.renderer.setPixelRatio(Math.max(window.devicePixelRatio, 2));
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        // this.renderer.outputColorSpace = THREE.SRGBColorSpace;
        container.appendChild(this.renderer.domElement);
        this.setEnvironment()

        const geometry = new THREE.BoxGeometry( 1, 1, 1 );
        const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
        const cube = new THREE.Mesh( geometry, material );
        this.scene.add( cube );
        cube.position.set(0, 1.6, -4)


        this.reticle = new THREE.Mesh(
            new THREE.RingGeometry(0.15, 0.2, 32).rotateX(- Math.PI / 2),
            new THREE.MeshBasicMaterial()
        );

        this.reticle.matrixAutoUpdate = false;
        this.reticle.visible = false;
        this.scene.add(this.reticle);

        const _this = this

        this.setupXR()
        this.showModel()

        window.addEventListener('resize', this.resize.bind(this))
        function animate() {
            requestAnimationFrame( animate );

            cube.rotation.x += 0.01;
            cube.rotation.y += 0.01;

            _this.renderer.render( _this.scene, _this.camera );
        }

        animate();
    }

    showModel() {
        this.initAR();


        const _this = this;

        this.loadingBar.visible = true


        const loader = new GLTFLoader()
        loader.load('./assets/models/wooden_table.glb', function(gltf) {
            _this.scene.add(gltf.scene)
            _this.chair = gltf.scene
            _this.chair.visible = false
            _this.loadingBar.visible = false
            _this.renderer.setAnimationLoop(_this.render.bind(_this))
        }, function (xhr) {

                _this.loadingBar.progress = (xhr.loaded / xhr.total);

            },
            // called when loading has errors
            function (error) {

                console.log('An error happened');

            })
    }

    setEnvironment() {
        const loader = new RGBELoader().setDataType(THREE.UnsignedByteType);
        const pmremGenerator = new THREE.PMREMGenerator(this.renderer);
        pmremGenerator.compileEquirectangularShader();

        // loader.load('ar-shop/venice_sunset_1k.hdr', (texture) => {
        //   const envMap = pmremGenerator.fromEquirectangular(texture).texture;
        //   pmremGenerator.dispose();

        //   self.scene.environment = envMap;

        // }, undefined, (err) => {
        //   console.error('An error occurred setting the environment');
        // });

        // const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
        // self.scene.add(directionalLight)

        const self = this;
    }


    resize() {
        this.camera.aspect = window.innerWidth / window.innerHeight
        this.camera.updateProjectionMatrix()
        this.renderer.setSize(window.innerWidth, window.innerHeight)
    }

    setupXR() {
        this.renderer.xr.enabled = true;

        if ('xr' in navigator) {

            navigator.xr.isSessionSupported('immersive-ar').then((supported) => {
                if (!supported) {
                    alert("Not supported xr.Pls use browser # or device #")
                }
            }).catch(() => alert("Not supported xr.Pls use browser # or device #"));

        }

        const self = this;

        this.hitTestSourceRequested = false;
        this.hitTestSource = null;

        function onSelect() {
            if (self.chair === undefined) return;

            if (self.reticle.visible) {
                self.chair.position.setFromMatrixPosition(self.reticle.matrix);
                self.chair.visible = true;
            }
        }

        this.controller = this.renderer.xr.getController(0);
        this.controller.addEventListener('select', onSelect);

        this.scene.add(this.controller);
    }

    initAR() {
        let currentSession = null;
        const self = this;

        const sessionInit = { requiredFeatures: ['hit-test'] };


        function onSessionStarted(session) {

            session.addEventListener('end', onSessionEnded);

            self.renderer.xr.setReferenceSpaceType('local');
            self.renderer.xr.setSession(session);

            currentSession = session;

        }

        function onSessionEnded() {

            currentSession.removeEventListener('end', onSessionEnded);

            currentSession = null;

            if (self.chair !== null) {
                self.scene.remove(self.chair);
                self.chair = null;
            }

            self.renderer.setAnimationLoop(null);

        }

        if (currentSession === null) {

            navigator.xr.requestSession('immersive-ar', sessionInit).then(onSessionStarted).catch(() => alert("not support"));

        } else {

            currentSession.end();

        }
    }

    requestHitTestSource() {
        const self = this;

        const session = this.renderer.xr.getSession();

        session.requestReferenceSpace('viewer').then(function (referenceSpace) {

            session.requestHitTestSource({ space: referenceSpace }).then(function (source) {

                self.hitTestSource = source;

            }).catch(() => alert("errir"));

        });

        session.addEventListener('end', function () {

            self.hitTestSourceRequested = false;
            self.hitTestSource = null;
            self.referenceSpace = null;

        });

        this.hitTestSourceRequested = true;

    }

    getHitTestResults(frame) {
        const hitTestResults = frame.getHitTestResults(this.hitTestSource);

        if (hitTestResults.length) {

            const referenceSpace = this.renderer.xr.getReferenceSpace();
            const hit = hitTestResults[0];
            const pose = hit.getPose(referenceSpace);

            this.reticle.visible = true;
            this.reticle.matrix.fromArray(pose.transform.matrix);

        } else {

            this.reticle.visible = false;

        }

    }

    render(timestamp, frame) {

        if (frame) {
            if (this.hitTestSourceRequested === false) this.requestHitTestSource()

            if (this.hitTestSource) this.getHitTestResults(frame);
        }

        this.renderer.render(this.scene, this.camera);

    }
}


export { App }